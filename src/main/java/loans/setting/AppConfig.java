package loans.setting;

import loans.scoring.Randomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public Randomizer randomizer() {
        return new Randomizer();
    }
}
