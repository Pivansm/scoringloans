package loans.domain.entity;

import lombok.Data;

import javax.persistence.Column;

@Data
public class Credit {
    private Long id;
    private Double limitCredit;
    private Double rate;
    private int term;
    private String type_credit;

}
