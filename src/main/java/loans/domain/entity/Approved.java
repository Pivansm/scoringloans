package loans.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Approved {

    @Id
    private Long id;
    private String message;
    private boolean completed;
    private Long offerId;

}
