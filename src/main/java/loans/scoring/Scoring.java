package loans.scoring;

public interface Scoring {
    public int age();
    public int ageCoefficient();
    public boolean creditApproval();
}
