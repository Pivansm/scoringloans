package loans.scoring.impl;

import loans.customException.WrongAgeException;
import loans.domain.entity.CreditOffer;
import loans.scoring.Randomizer;
import loans.scoring.Scoring;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Range;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Range;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

@Slf4j
public class ScoringImpl implements Scoring {

    private CreditOffer creditOffer;
    @Autowired
    Randomizer randomizer;

    public ScoringImpl(CreditOffer creditOffer) {
        this.creditOffer = creditOffer;
    }

    @Override
    public int age() {
        LocalDate birthDate = creditOffer.getClient().getBirthDate();
        LocalDate currentDate=LocalDate.now();
        if((birthDate!=null) && (currentDate!=null)){
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    @Override
    public int ageCoefficient() {
        Range<Integer> range1 = Range.between(25, 55);
        Range<Integer> range2 = Range.between(18, 24);
        Range<Integer> range3 = Range.between(56, 75);

        if (range1.contains(age())) {
            return 1;
        } else if (range2.contains(age())) {
            return 2;
        } else if (range3.contains(age())) {
            return 3;
        } else try {
            throw new WrongAgeException("Возраст кредитования должен быть от 18 до 75 лет включительно");
        } catch (WrongAgeException e) {
            e.printStackTrace();
            log.error("err:=" + e);
        }
        return 0;
    }

    @Override
    public boolean creditApproval() {
        switch (ageCoefficient()) {
            case 1:
                break;
            case 2:
            case 3:
                if(randomizer.randomize()==true){
                    return true;
                }break;
        }
        return false;
    }
}
